package com.apps.myapplication.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.apps.myapplication.data.remote.dto.WeatherAPI
import com.apps.myapplication.data.remote.dto.WeatherDataDto
import com.apps.myapplication.utils.Constant
import com.apps.myapplication.utils.Resource
import com.apps.myapplication.utils.SpUtil
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class WeatherRepositoryImpl @Inject constructor(
    val api: WeatherAPI,
    val spUtil: SpUtil
) : WeatherRepository {

    override suspend fun weatherApiData(map:HashMap<String,String>):Resource<WeatherDataDto>
    {

        return try {
            Resource.Loading(null)
            val data=api.weatherApiData(map=map)
            Resource.Success(data)
        }  catch(e: HttpException) {
            Resource.Error<WeatherDataDto>(e.localizedMessage ?: "An unexpected error occured")
        } catch(e: IOException) {
            Resource.Error<WeatherDataDto>("Couldn't reach server. Check your internet connection.")
        }catch (e : Exception){
            Resource.Error<WeatherDataDto>(e.localizedMessage ?: "An unexpected error occured")

        }
    }

    override suspend fun saveLastCityPosition(pos: Int) {
        spUtil.save(Constant.LastCity,pos)
    }

    override fun getLastSavedCity(): Int {
        return spUtil.getValueInt(Constant.LastCity) ?:0
    }
}