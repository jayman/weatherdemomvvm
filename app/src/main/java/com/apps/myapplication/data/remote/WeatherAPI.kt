package com.apps.myapplication.data.remote.dto

import retrofit2.http.GET
import retrofit2.http.QueryMap

interface WeatherAPI {
   // q="", key=""
    @GET("current.json")
    suspend fun weatherApiData(@QueryMap map :Map<String,String>): WeatherDataDto
}