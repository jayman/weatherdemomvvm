package com.apps.myapplication.data.remote.dto

data class WeatherOtherInfo(
    val primaryText:String="",
    val secondaryText:String=""
)
