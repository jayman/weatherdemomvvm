package com.apps.myapplication.data.remote.dto

data class WeatherData(
    val city:String="",
    val lastUpdateDate:String="",
    val conditionImageUrl:String="",
    val conditionState:String="",
    val currentTemp:String="°C",
    val otherInfo: List<WeatherOtherInfo> = emptyList()
)
