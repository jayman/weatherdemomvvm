package com.apps.myapplication.data.remote.dto

import com.apps.myapplication.utils.toDDMMYYhhmma
import com.google.gson.annotations.SerializedName
import java.util.Date

data class WeatherDataDto(
    @SerializedName("location" ) var location : Location? = Location(),
    @SerializedName("current"  ) var current  : Current?  = Current()
) {

//    override fun toString(): String {
//        return "WeatherData(location=$location, current=$current)"
//    }
}

data class Location (

    @SerializedName("name"            ) var name           : String? = null,
    @SerializedName("region"          ) var region         : String? = null,
    @SerializedName("country"         ) var country        : String? = null,
    @SerializedName("lat"             ) var lat            : Double? = null,
    @SerializedName("lon"             ) var lon            : Double? = null,
    @SerializedName("tz_id"           ) var tzId           : String? = null,
    @SerializedName("localtime_epoch" ) var localtimeEpoch : Long?    = null,
    @SerializedName("localtime"       ) var localtime      : String? = null

) {
    override fun toString(): String {
        return "Location(name=$name, region=$region, country=$country, lat=$lat, lon=$lon, tzId=$tzId, localtimeEpoch=$localtimeEpoch, localtime=$localtime)"
    }
}

data class Condition (

    @SerializedName("text" ) var text : String? = null,
    @SerializedName("icon" ) var icon : String? = null,
    @SerializedName("code" ) var code : Int?    = null

) {
    override fun toString(): String {
        return "Condition(text=$text, icon=$icon, code=$code)"
    }
}

data class Current (

    @SerializedName("last_updated_epoch" ) var lastUpdatedEpoch : Int?       = null,
    @SerializedName("last_updated"       ) var lastUpdated      : String?    = null,
    @SerializedName("temp_c"             ) var tempC            : Double?       = null,
    @SerializedName("temp_f"             ) var tempF            : Double?    = null,
    @SerializedName("is_day"             ) var isDay            : Int?       = null,
    @SerializedName("condition"          ) var condition        : Condition? = Condition(),
    @SerializedName("wind_mph"           ) var windMph          : Double?    = null,
    @SerializedName("wind_kph"           ) var windKph          : Double?    = null,
    @SerializedName("wind_degree"        ) var windDegree       : Int?       = null,
    @SerializedName("wind_dir"           ) var windDir          : String?    = null,
    @SerializedName("pressure_mb"        ) var pressureMb       : Double?       = null,
    @SerializedName("pressure_in"        ) var pressureIn       : Double?    = null,
    @SerializedName("precip_mm"          ) var precipMm         : Double?       = null,
    @SerializedName("precip_in"          ) var precipIn         : Double?       = null,
    @SerializedName("humidity"           ) var humidity         : Int?       = null,
    @SerializedName("cloud"              ) var cloud            : Int?       = null,
    @SerializedName("feelslike_c"        ) var feelslikeC       : Double?    = null,
    @SerializedName("feelslike_f"        ) var feelslikeF       : Double?       = null,
    @SerializedName("vis_km"             ) var visKm            : Double?       = null,
    @SerializedName("vis_miles"          ) var visMiles         : Double?       = null,
    @SerializedName("uv"                 ) var uv               : Double?       = null,
    @SerializedName("gust_mph"           ) var gustMph          : Double?    = null,
    @SerializedName("gust_kph"           ) var gustKph          : Double?    = null

) {
    override fun toString(): String {
        return "Current(lastUpdatedEpoch=$lastUpdatedEpoch, lastUpdated=$lastUpdated, tempC=$tempC, tempF=$tempF, isDay=$isDay, condition=$condition, windMph=$windMph, windKph=$windKph, windDegree=$windDegree, windDir=$windDir, pressureMb=$pressureMb, pressureIn=$pressureIn, precipMm=$precipMm, precipIn=$precipIn, humidity=$humidity, cloud=$cloud, feelslikeC=$feelslikeC, feelslikeF=$feelslikeF, visKm=$visKm, visMiles=$visMiles, uv=$uv, gustMph=$gustMph, gustKph=$gustKph)"
    }
}

fun WeatherDataDto.toWeatherDate():WeatherData{
    val date:Long=location?.localtimeEpoch ?: Date().time
    return WeatherData(
        city = location?.name ?:" ",
        lastUpdateDate="Updated at: "+ date.toDDMMYYhhmma(),
        conditionImageUrl="http:"+current?.condition?.icon.toString(),
        conditionState = current?.condition?.text ?: "",
        currentTemp=(current?.tempC ?: 0.0).toString()+"°C",
        otherInfo = ArrayList<WeatherOtherInfo>().apply {
            add(WeatherOtherInfo("Wind Flow",current?.windKph?.toString()+" km/hr"))
            add(WeatherOtherInfo("Wind Dir",current?.windDir?.toString()+""))
            add(WeatherOtherInfo("Wind Degree",current?.windDegree?.toString()+""))

            add(WeatherOtherInfo("Pressure",current?.pressureIn?.toString()+" In"))
            add(WeatherOtherInfo("Humidity",current?.humidity?.toString()+" %"))
            add(WeatherOtherInfo("cloud",current?.cloud?.toString()+" %"))
        }
    )
}

