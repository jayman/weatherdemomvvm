package com.apps.myapplication.data.repository

import com.apps.myapplication.data.remote.dto.WeatherDataDto
import com.apps.myapplication.utils.Resource

interface WeatherRepository {

    suspend fun weatherApiData(map: HashMap<String, String>): Resource<WeatherDataDto>

    suspend fun saveLastCityPosition(pos:Int):Unit
    fun getLastSavedCity(): Int
}