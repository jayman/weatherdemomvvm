package com.apps.myapplication.utils

import java.text.SimpleDateFormat
import java.util.Date

fun Long.toDDMMYYhhmma():String{
    val date = Date(this*1000L)
    val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm a")
    return dateFormat.format(date)

}