package com.apps.myapplication.di

import com.apps.myapplication.data.remote.dto.WeatherAPI
import com.apps.myapplication.data.repository.WeatherRepositoryImpl
import com.apps.myapplication.data.repository.WeatherRepository
import com.apps.myapplication.utils.Constant
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.apps.myapplication.utils.SpUtil

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideWeatherApi(): WeatherAPI {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Constant.BASE_URL)
            .build()
            .create( WeatherAPI::class.java)
    }

    @Provides
    @Singleton
    fun provideWeatherApiRepository(aPi:  WeatherAPI, sharedPref:SpUtil): WeatherRepository {
        return WeatherRepositoryImpl(aPi,sharedPref)
    }
    @Provides
    fun providesSharedPreferences(context: Application): SharedPreferences {
        return context.getSharedPreferences("Temp_Db", Context.MODE_PRIVATE)
    }
    @Provides
    fun providesSharedPreferencesUtil(sharedPref: SharedPreferences): SpUtil {
        return SpUtil(sharedPref)
    }

}