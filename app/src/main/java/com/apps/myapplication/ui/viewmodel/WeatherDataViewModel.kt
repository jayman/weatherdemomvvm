package com.apps.myapplication.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.apps.myapplication.BuildConfig
import com.apps.myapplication.data.remote.dto.WeatherDataDto
import com.apps.myapplication.data.remote.dto.toWeatherDate
import com.apps.myapplication.data.repository.WeatherRepository
import com.apps.myapplication.ui.WeatherState
import com.apps.myapplication.utils.Constant
import com.apps.myapplication.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import javax.annotation.meta.When
import javax.inject.Inject
@HiltViewModel
class WeatherDataViewModel @Inject constructor(
    val repository: WeatherRepository
) :ViewModel(){
    private val _weatherDataDto =  MutableLiveData<WeatherState>()
    val weatherDataDto :LiveData<WeatherState> = _weatherDataDto


    private val _lastCity =  MutableLiveData<Int>()
    val lastCity :LiveData<Int> = _lastCity

    suspend fun saveLastCity(pos:Int){

        repository.saveLastCityPosition(pos)
    }

    fun getLastSaveCity(){
        val pos= repository.getLastSavedCity()
        _weatherDataDto.postValue(WeatherState.CurrentPos(pos))
    }

    suspend fun getWeatherData(cityName:String){
        try {
            val map=HashMap<String,String>()


            map["key"] = BuildConfig.API_KEY
            map["q"] = cityName
            val result =repository.weatherApiData(map)
           // _weatherDataDto.postValue(data)
            when (result) {
                is Resource.Success -> {
                    delay(1000)
                    pushWeatherDataEven(WeatherState.Success(result.data!!.toWeatherDate()))

                }
                is Resource.Error -> {
                    pushWeatherDataEven(WeatherState.Error(result.message ?: "An unexpected error occured"))
                }
                is Resource.Loading -> {
                    pushWeatherDataEven(WeatherState.Loading)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun pushWeatherDataEven(state:WeatherState){
        _weatherDataDto.postValue(state)

    }
}