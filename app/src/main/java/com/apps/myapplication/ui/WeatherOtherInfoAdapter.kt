package com.apps.myapplication.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.apps.myapplication.R
import com.apps.myapplication.data.remote.dto.WeatherOtherInfo

class WeatherOtherInfoAdapter(var context: Context) : RecyclerView.Adapter<WeatherOtherInfoAdapter.ViewHolder>() {

    var dataList = emptyList<WeatherOtherInfo>()

    internal fun setDataList(dataList: List<WeatherOtherInfo>) {
        this.dataList = dataList
    }

    // Provide a direct reference to each of the views with data items

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView
        var desc: TextView

        init {
            title = itemView.findViewById(R.id.tvTitle)
            desc = itemView.findViewById(R.id.tvDeails)
        }

    }

    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherOtherInfoAdapter.ViewHolder {

        // Inflate the custom layout
        var view = LayoutInflater.from(parent.context).inflate(R.layout.raw_other_wather_info, parent, false)
        return ViewHolder(view)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(holder: WeatherOtherInfoAdapter.ViewHolder, position: Int) {

        // Get the data model based on position
        var data = dataList[position]

        // Set item views based on your views and data model
        holder.title.text = data.primaryText
        holder.desc.text = data.secondaryText

    }

    //  total count of items in the list
    override fun getItemCount() = dataList.size
}