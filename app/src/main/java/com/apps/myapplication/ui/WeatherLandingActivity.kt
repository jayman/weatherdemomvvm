package com.apps.myapplication.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.ui.AppBarConfiguration
import androidx.recyclerview.widget.GridLayoutManager
import com.apps.myapplication.R
import com.apps.myapplication.data.remote.dto.WeatherOtherInfo
import com.apps.myapplication.databinding.ActivityWeatherLandingBinding
import com.apps.myapplication.ui.viewmodel.WeatherDataViewModel
import com.apps.myapplication.utils.toDDMMYYhhmma
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import java.util.*


@AndroidEntryPoint
class WeatherLandingActivity : AppCompatActivity() {

    private lateinit var binding: ActivityWeatherLandingBinding
    private val viewModel:WeatherDataViewModel by viewModels()
    private lateinit var  adapter: WeatherOtherInfoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityWeatherLandingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

      //  getWeatherData()
        adapter = WeatherOtherInfoAdapter(applicationContext)
        binding.recOtherInfo.layoutManager=GridLayoutManager(this,3)

        binding.recOtherInfo.adapter=adapter

        val adapterSpinner = ArrayAdapter.createFromResource(
            this@WeatherLandingActivity,
            R.array.cityname, android.R.layout.simple_spinner_item
        )
        adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        binding.spSearchCity.adapter = adapterSpinner

        viewModel.getLastSaveCity()

        viewModel.lastCity.observe(this, Observer {


        })

        binding.spSearchCity.onItemSelectedListener=object :AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val pos=p2
                lifecycleScope.launch {
                    viewModel.saveLastCity(pos)
                }
                getWeatherData()
               /* if (viewModel.lastCity.value!= pos){
                    viewModel.currentPos=pos
                    getWeatherData()
                }*/
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

        }
        viewModel.weatherDataDto.observe(this, Observer {
            Log.e("TAG", "onCreate: ${it.toString()}" )
            val state=it
            when(state){
                is WeatherState.CurrentPos -> {
                    binding.spSearchCity.setSelection(state.position)
                }
                is WeatherState.Error ->{
                    binding.pbProgress.visibility=View.GONE
                    Toast.makeText(this,state.error,Toast.LENGTH_LONG).show()
                }
                WeatherState.Loading -> {
                    binding.pbProgress.visibility=View.VISIBLE

                }
                is WeatherState.Success -> {
                    binding.pbProgress.visibility=View.GONE
                    binding.tvCityTitle.text= state.data.city
                    binding.updatedAt.text= state.data.lastUpdateDate
                    binding.conditionStatus.text=state.data.conditionState



                    binding.temp.text=state.data.currentTemp
                    adapter.setDataList(state.data.otherInfo)
                    adapter.notifyDataSetChanged()

                    Glide.with(this@WeatherLandingActivity)
                        .load(state.data.conditionImageUrl)
                        .into(binding.imgCondition)
                }
            }


 /*           val data=it

            val date=data.location?.localtimeEpoch ?: Date().time
            binding.tvCityTitle.text= data.location?.name +" "+ data.location?.country
            binding.updatedAt.text= "Updated at: "+ date.toDDMMYYhhmma()
            binding.conditionStatus.text=data.current?.condition?.text



            binding.temp.text=data.current?.tempC.toString() +"°C"
            val listOtheInfo=ArrayList<WeatherOtherInfo>()
            listOtheInfo.add(WeatherOtherInfo("Wind Flow",data.current?.windKph?.toString()+" km/hr"))
            listOtheInfo.add(WeatherOtherInfo("Wind Dir",data.current?.windDir?.toString()+""))
            listOtheInfo.add(WeatherOtherInfo("Wind Degree",data.current?.windDegree?.toString()+""))

            listOtheInfo.add(WeatherOtherInfo("Pressure",data.current?.pressureIn?.toString()+" In"))
            listOtheInfo.add(WeatherOtherInfo("Humidity",data.current?.humidity?.toString()+" %"))
            listOtheInfo.add(WeatherOtherInfo("cloud",data.current?.cloud?.toString()+" %"))
            adapter.setDataList(listOtheInfo)
            adapter.notifyDataSetChanged()

            Glide.with(this@WeatherLandingActivity)
                .load("http:"+data?.current?.condition?.icon.toString())
                .into(binding.imgCondition)*/
        })

    }

    private fun getWeatherData() {
        lifecycleScope.launch {
            val getcity= binding.spSearchCity.selectedItem.toString()
            Log.e("getCity",getcity+"<<")

            viewModel.getWeatherData(getcity)
        }
    }


}