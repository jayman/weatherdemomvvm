package com.apps.myapplication.ui

import androidx.appcompat.app.AppCompatActivity
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.MotionEvent
import android.view.View
import android.view.WindowInsets
import android.widget.LinearLayout
import android.widget.TextView
import com.apps.myapplication.R
import com.apps.myapplication.databinding.ActivitySplash2Binding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class ActivitySplash : AppCompatActivity() {

    private lateinit var binding: ActivitySplash2Binding



    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySplash2Binding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.setDisplayHomeAsUpEnabled(false)

// Start a coroutine that will start the main activity after 3 seconds.
        GlobalScope.launch {
            delay(2000)
            startActivity(Intent(this@ActivitySplash, WeatherLandingActivity::class.java))
            finish()
        }

    }











}