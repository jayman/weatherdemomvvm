package com.apps.myapplication.ui

import com.apps.myapplication.data.remote.dto.WeatherData
import com.apps.myapplication.data.remote.dto.WeatherDataDto

sealed class WeatherState {
    object Loading : WeatherState()
    data class Success(val data: WeatherData) : WeatherState()
    data class Error(val error: String="") : WeatherState()
    data class CurrentPos(val position: Int=0) : WeatherState()

}

/*    val isLoading: Boolean = false,
    val data: WeatherData = WeatherData(),
    val error: String = "",
    var pos:Int=0
)*/
